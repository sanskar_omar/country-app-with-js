// Fetching data from API

const xml = new XMLHttpRequest()
xml.open('GET', 'https://restcountries.com/v3.1/all', true);
xml.onload = function () {
    if (this.status === 200) {
        let data = JSON.parse(this.responseText);
        // console.log("Recieved Data: " + data)
        // I have been unable to get data from API due to some issue at their end
        // So i don't know yet what all keys i recieve from this API, hence for now
        // I am logging it out on console
    }
    else {
        console.error("Error Occured! Code:" + this.status)
    }
}
xml.send()
console.log("Sent Request")

// Say we have countries list from API something of form 
let countries = [
    {
        "Name": "CountryName",
        "flag": "https://upload.wikimedia.org/wikipedia/commons/2/20/Flag_of_the_United_States_%281912%E2%80%931959%29.jpg",
        "Currency": "INR",
        "DateAndTime": "25 dec 2002, 14:05Am",
        "Location": "27.2046° N, 77.4977° E",
        "alpha3code": "CODE",
    },
    {
        "Name": "Nepal",
        "flag": "https://upload.wikimedia.org/wikipedia/commons/2/20/Flag_of_the_United_States_%281912%E2%80%931959%29.jpg",
        "Currency": "INR",
        "DateAndTime": "25 dec 2002, 14:05Am",
        "Location": "27.2046° N, 77.4977° E",
    }
]


let insert_data = ""

function countryListRender() {
    for (let i = 0; i < countries.length; i++) {
        let country = countries[i]
        insert_data += `<div class="card text-white bg-dark mb-3 shadow-lg">
    <div class="row g-0">
        <div class="col-md-4">
            <img
                src=${country.flag}
                class="img-fluid rounded-start"
                alt="${country.Name}'s Flag"
            />
        </div>
        <div class="col-md-8 card-body">
            <h5 class="card-title">${country.Name}</h5>
            <p class="card-text">
                Currency: ${country.Currency}<br />Current Date and Time: ${country.DateAndTime}
            </p>
            <a href="https://maps.google.com/?q=${country.Location}"><button type="button" class="btn btn-info">Show Map</button></a>
            <a href="./Detail.html"><button type="button" class="btn btn-info">Detail</button></a>
        </div >
    </div >
</div >`
    }
}
countryListRender()

// Placeholder country cards as API data is not avaliable right now
for (let i = 0; i < 10; i++) {
    let country = countries[i]
    insert_data += `<div class="card bg-secondary mb-3 shadow-lg" aria-hidden="true">
    <div class="row g-0">
      <div class="col-md-4">
        <img src="" class="img-fluid rounded-start" alt="" />
      </div>
      <div class="col-md-8 card-body">
        <h5 class="card-title placeholder-glow">
          <span class="placeholder col-6"></span>
        </h5>
        <p class="card-text placeholder-glow">
          <span class="placeholder col-5"></span>
          <br />
          <span class="placeholder col-4"></span>
          <span class="placeholder col-3"></span>
          <span class="placeholder col-3"></span>
        </p>
        <button
          type="button"
          class="btn btn-info disabled placeholder col-3"
        ></button>
        <button
          type="button"
          class="btn btn-info disabled placeholder col-2"
        ></button>
      </div>
    </div>
  </div>`
}

const content = document.getElementById("Content")
content.innerHTML = insert_data


// Implementing search bar

let keyword = ""
//  Filtering countries according to keyword
function filterCountries() {
    let filtered_countries = countries.filter(country =>
        country.Name.toLowerCase().includes(keyword) ||
        country.Currency.toLowerCase().includes(keyword)
    )
    return filtered_countries
}

// Function Used in Search bar
function searchCountry() {
    const search = document.getElementById("search")
    const searchWord = search.value
    keyword = searchWord.toLowerCase()
    countries = filterCountries();
    countryListRender();
}


